
const button = document.getElementById('button')
const scoreElement = document.getElementById('score_element')

if (typeof (localStorage) == 'undefined'){
    console.log('Your browser not support localstorage')
}

if (localStorage.getItem('score')){
    scoreElement.innerText = localStorage.getItem('score')

} else {
    localStorage.setItem('score', '0')
}
//DECLARAMOS LA ACCIÓN DEL ALMACENAMIENTO QUE VA A SUMAR DE MANERA LOCAL CADA VEZ QUE APLIQUEMOS EL BOTON
button.onclick = () =>{
    const currentScore = localStorage.getItem('score')
    localStorage.setItem('score', parseInt(currentScore) + 1)
    scoreElement.innerText = localStorage.getItem('score')
}
