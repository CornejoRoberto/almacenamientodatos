import { Component } from '@angular/core';
import { CargarScriptsService } from 'src/app/cargar-scripts.service';

@Component({
  selector: 'app-pagina1',
  templateUrl: './pagina1.component.html',
  styleUrls: ['./pagina1.component.css']
})
//CARGAMOS EL SCRIPT PARA EJECUTAR EL JAVA CON EL ALMACENAMIENTO DE DATOS
export class Pagina1Component {
  constructor(private _CargaScripts:CargarScriptsService)
  {
    _CargaScripts.Carga(["Storage"]);
  }

}
